package cassdemo.backend;

public class Nick {
    private String nick;
    private Integer nodeId;

    public Nick() {
    }

    public Nick(String nick, Integer nodeId) {
        this.nick = nick;
        this.nodeId = nodeId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }
}
