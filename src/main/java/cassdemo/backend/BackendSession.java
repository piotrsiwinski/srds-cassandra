package cassdemo.backend;

import com.datastax.driver.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/*
 * For error handling done right see: 
 * https://www.datastax.com/dev/blog/cassandra-error-handling-done-right
 * 
 * Performing stress tests often results in numerous WriteTimeoutExceptions, 
 * ReadTimeoutExceptions (thrown by Cassandra replicas) and 
 * OpetationTimedOutExceptions (thrown by the client). Remember to retry
 * failed operations until success (it can be done through the RetryPolicy mechanism:
 * https://stackoverflow.com/questions/30329956/cassandra-datastax-driver-retry-policy )
 */

public class BackendSession {

    private static final Logger logger = LoggerFactory.getLogger(BackendSession.class);
    private static final List<String> nicks = Arrays.asList("piotr", "mateusz", "byku");
    public static final Integer THIS_NODE_ID = 1;

    private Session session;

    public BackendSession(String contactPoint, String keyspace) throws BackendException {

        Cluster cluster = Cluster.builder().addContactPoint(contactPoint).build();
        try {
            session = cluster.connect(keyspace);
        } catch (Exception e) {
            throw new BackendException("Could not connect to the cluster. " + e.getMessage() + ".", e);
        }
        prepareStatements();
    }

    private static PreparedStatement SELECT_ALL_FROM_USERS;
    private static PreparedStatement SELECT_NICK_FROM_USERS_BY_NAME;
    private static PreparedStatement INSERT_INTO_NICKS;
    private static PreparedStatement DELETE_FROM_NICKS;

    private static PreparedStatement INSERT_INTO_USERS;
    private static PreparedStatement DELETE_ALL_FROM_USERS;

    private static final String USER_FORMAT = "- %-10s  %-16s %-10s %-10s\n";
    // private static final SimpleDateFormat df = new
    // SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void zadanie3() throws BackendException, InterruptedException {
        for (; ; ) {
            Random random = new Random();
            int nickId = random.nextInt(nicks.size());
            String randomNick = nicks.get(nickId);
            logger.info("Pobieram nick {} ", randomNick);
            Nick nick = selectNick(randomNick);
            if (Objects.nonNull(nick.getNick())) {
                logger.info("nick {} zajety przez {}", nick.getNick(), nick.getNodeId());
                continue;
            }
            nick.setNick(randomNick);
            nick.setNodeId(THIS_NODE_ID);

            insertNick(nick);
            Nick insertedNick = selectNick(randomNick);

            if (Objects.isNull(insertedNick.getNodeId())) {
                logger.info("insertedNick with NULL NODEID");
                continue;
            }

            logger.info(
                    insertedNick.getNodeId().equals(THIS_NODE_ID)
                            ? "PO INSERT: Nick zajety przeze mnie"
                            : "!!!!!!!!!!!!PO INSERT: Nick zajety przez node {}", insertedNick.getNodeId());


            deleteNick(randomNick);
            Thread.sleep(1);
        }

    }

    private void deleteNick(String nickToDelete) {
        BoundStatement bs = new BoundStatement(DELETE_FROM_NICKS);
        bs.bind(nickToDelete);
        ResultSet execute = session.execute(bs);
        logger.info("Deleted nick {}", nickToDelete);
    }

    private void insertNick(Nick nick) throws BackendException {
        BoundStatement bs = new BoundStatement(INSERT_INTO_NICKS);
        bs.bind(nick.getNick(), nick.getNodeId());
        ResultSet rs;
        try {
            rs = session.execute(bs);
        } catch (Exception e) {
            throw new BackendException("Could not perform a query. " + e.getMessage() + ".", e);
        }
        logger.info("Inserted nick {}, by node {}", nick.getNick(), nick.getNodeId());
    }

    public Nick selectNick(String nick) throws BackendException {
        BoundStatement bs = new BoundStatement(SELECT_NICK_FROM_USERS_BY_NAME);
        bs.bind(nick);
        ResultSet rs;
        try {
            rs = session.execute(bs);
        } catch (Exception e) {
            throw new BackendException("Could not perform a query. " + e.getMessage() + ".", e);
        }
        Nick result = new Nick();
        for (Row row : rs) {
            result.setNick(row.getString("nick"));
            result.setNodeId(row.getInt("nodeId"));
        }
        return result;
    }

    private void prepareStatements() throws BackendException {
        try {

            SELECT_NICK_FROM_USERS_BY_NAME = session.prepare("SELECT nick, nodeid FROM NICKS WHERE nick=?")
                    .setConsistencyLevel(ConsistencyLevel.QUORUM);
            INSERT_INTO_NICKS = session.prepare("INSERT INTO NICKS(nick, nodeId) values(?, ?);")
                    .setConsistencyLevel(ConsistencyLevel.QUORUM);
            DELETE_FROM_NICKS = session.prepare("DELETE FROM NICKS WHERE NICK= ?")
                    .setConsistencyLevel(ConsistencyLevel.QUORUM);


            INSERT_INTO_USERS = session
                    .prepare("INSERT INTO users (companyName, name, phone, street) VALUES (?, ?, ?, ?);")
                    .setConsistencyLevel(ConsistencyLevel.QUORUM);
            SELECT_ALL_FROM_USERS = session.prepare("SELECT * FROM users;")
                    .setConsistencyLevel(ConsistencyLevel.ALL);

            DELETE_ALL_FROM_USERS = session.prepare("TRUNCATE users;")
                    .setConsistencyLevel(ConsistencyLevel.QUORUM);

        } catch (Exception e) {
            throw new BackendException("Could not prepare statements. " + e.getMessage() + ".", e);
        }

        logger.info("Statements prepared");
    }

    public String selectAll() throws BackendException {
        StringBuilder builder = new StringBuilder();
        BoundStatement bs = new BoundStatement(SELECT_ALL_FROM_USERS);

        ResultSet rs = null;

        try {
            rs = session.execute(bs);
        } catch (Exception e) {
            throw new BackendException("Could not perform a query. " + e.getMessage() + ".", e);
        }

        for (Row row : rs) {
            String rcompanyName = row.getString("companyName");
            String rname = row.getString("name");
            int rphone = row.getInt("phone");
            String rstreet = row.getString("street");

            builder.append(String.format(USER_FORMAT, rcompanyName, rname, rphone, rstreet));
        }

        return builder.toString();
    }

    public void upsertUser(String companyName, String name, int phone, String street) throws BackendException {
        BoundStatement bs = new BoundStatement(INSERT_INTO_USERS);
        bs.bind(companyName, name, phone, street);

        try {
            session.execute(bs);
        } catch (Exception e) {
            throw new BackendException("Could not perform an upsert. " + e.getMessage() + ".", e);
        }

        logger.info("User " + name + " upserted");
    }

    public void deleteAll() throws BackendException {
        BoundStatement bs = new BoundStatement(DELETE_ALL_FROM_USERS);

        try {
            session.execute(bs);
        } catch (Exception e) {
            throw new BackendException("Could not perform a delete operation. " + e.getMessage() + ".", e);
        }

        logger.info("All users deleted");
    }

    protected void finalize() {
        try {
            if (session != null) {
                session.getCluster().close();
            }
        } catch (Exception e) {
            logger.error("Could not close existing cluster", e);
        }
    }

}
